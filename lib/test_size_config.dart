import 'package:flutter/material.dart';
import 'package:size_config/size_config.dart';
class TestPageConfig extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Container(
            height: SizeConfig.blockSizeVertical * 20,
            width: double.infinity,
            color: Colors.orange,
            child: FittedBox(child: Text('AAaaaaaaaaaaaaaaaaaaaaaaaalllllllllllllllllllllllll',
            style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal * 20),)),
          ),
        ),
      ),
    );
  }
}
